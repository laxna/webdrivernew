package session0702;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class practivexpath0702 {
 static WebDriver driver;
	
 public static void main(String[] args) {
		
  login();
  goToCreateContactsPages();
  
  createContacts();
  
  verifyContactInformation();
  
  logout();
  

	}
 
 private static void login() {
		driver = new FirefoxDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60,TimeUnit.SECONDS);
		driver.get("http://encreo.com/crm/");
		driver.findElement(By.xpath("//input[@name ='username']")).sendKeys("crmuser");
		driver.findElement(By.xpath("//input[@name ='password']")).sendKeys("crmuser");
		driver.findElement(By.xpath("//button[text() ='Sign in']")).click();
		System.out.println("Login done");
		
	}

 
 private static void goToCreateContactsPages() {
  
	 driver.findElement(By.xpath("//Strong[text()='Contacts'][1]")).click();
	 System.out.println("contacts selected");
	 driver.findElement(By.xpath("//strong[text() = 'Add Contact']")).click();
	 System.out.println("Click AddContat");
	 
}

private static void createContacts() {
	
	driver.findElement(By.xpath("//input[@name ='firstname']")).sendKeys("Pankaj");
	driver.findElement(By.xpath("//input[@name='lastname']")).sendKeys("bhatti");
	driver.findElement(By.xpath("//input[@name ='mobile']")).sendKeys("2144444444");
	//driver.findElement(By.xpath("//input[@name ='emailoptout']")).click();
	driver.findElement(By.xpath("//input[@type='checkbox'][@name ='emailoptout']")).click();
	
	
	driver.findElement(By.xpath("//textarea[@name ='mailingstreet']")).sendKeys("9400 independence");
	
	driver.findElement(By.xpath("(//strong[text()='Save'])[2]")).click();
	
	
	System.out.println("Contact saved");
	
	
	
}

private static void verifyContactInformation() {
	// TODO Auto-generated method stub
	
}

private static void logout() {
	// TODO Auto-generated method stub

	driver.findElement(By.xpath("//strong[text()='Demo']")).click();
	driver.findElement(By.xpath("(//a[text()='Sign Out'])[1]")).click();
		System.out.println("Signout  is done");

}



}
