package session09;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CreateProductTest {

	WebDriver driver;
	// Test data variables
	
	String appURL  = "http://encreo.com/crm/index.php";
	String UserName = "crmuser";
	String Password = "crmuser";
    String Productname = "CRM version 5x";	
    String MfrPart = "5x";
    String ProductPrice ="150";
	
		
	//Xpaths
	String Xpath_UsernameTextbox="//input[@type='text'][@placeholder='Username']";
	String Xpath_PasswordTextbox = "//input[@type='password'][@placeholder='Password']";
	String Xpath_SigninButton = "//button[text()='Sign in']";
	String Xpath_ProductTab = "(//strong[text()='Products)[2]']";
	String Xpath_AddProductButton = "//strong[text()='Add Product']";
	String xpath_ProductNameTextbox = "//input[@type='text'][@name='productname']";
	String xpath_MfrPArtTextbox = "//input[@type='text'][@name='mfr_part_no']";
	String xpath_ProductPriceTexbox ="//input[@type='text'][@name='unit_price']";
	String xpath_SaveButton = "//strong[text()='Save']";
	String xpath_UserMenulink = "//strong[text()='Save']";
	String xpath_SignoutLink ="//a[text()='Sign Out']";

	private String xpath_ProductsTab;

	private String xpath_AddProductButton;

	private String xpath_ProductnameTextbox;

	private String xpath_ProductPriceTextbox;
	
	
	
	@Before
	public void login() {	
	
		System.setProperty("webdriver.chrome.driver","/Users/lakshanamishra/Documents/workspace/webdriver/lib/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.get(appURL);
		driver.findElement(By.xpath(Xpath_UsernameTextbox)).sendKeys(UserName);
		//driver.findElement(By.xpath(xpath_UsernameTextbox)).sendKeys(username);
		driver.findElement(By.xpath(Xpath_PasswordTextbox)).sendKeys(Password);

		driver.findElement(By.xpath(Xpath_SigninButton)).click();		
	}
	
		@After
		public void logout() {
			driver.findElement(By.xpath(xpath_UserMenulink)).click();
			driver.findElement(By.xpath(xpath_SignoutLink)).click();

		
		}
		
		@Test
		public void createProductTest() {
			
		driver.findElement(By.xpath(Xpath_ProductTab)).click();
		
		driver.findElement(By.xpath(xpath_AddProductButton)).click();
		driver.findElement(By.xpath(xpath_ProductnameTextbox)).sendKeys(Productname);
		driver.findElement(By.xpath(xpath_ProductPriceTextbox)).sendKeys(ProductPrice);
		driver.findElement(By.xpath(xpath_SaveButton)).click();

		
		
		
		
		
		
		
	}

}
