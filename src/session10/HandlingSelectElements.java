package session10;

import static org.junit.Assert.*;

import org.junit.Test;
	
	
		import static org.junit.Assert.*;

		import java.util.concurrent.TimeUnit;

		import org.junit.Before;
		import org.junit.Test;
		import org.openqa.selenium.By;
		import org.openqa.selenium.WebDriver;
		import org.openqa.selenium.WebElement;
		import org.openqa.selenium.chrome.ChromeDriver;
		import org.openqa.selenium.support.ui.Select;


		public class HandlingSelectElements {
			WebDriver driver; 
			String appURL="http://encreo.com/in/contact.html";
			String xpath_SalutationListbox = "//select[@name='salutation']";

			@Before
			public void setUp() throws Exception {
				System.setProperty("webdriver.chrome.driver" ,"/Users/lakshanamishra/Documents/workspace/webdriver/lib/chromedriver");
				driver = new ChromeDriver(); 
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				driver.get(appURL);
			}

			@Test
			public void test() {
				
				// fetch the web element using driver.findElement method
				WebElement listElement =driver.findElement(By.xpath(xpath_SalutationListbox));
				
				//listElement.clear();
				//listElement.sendKeys("");
				//listElement.click();
				
				//create a SELECT class object by passing the web element
				Select selectElement = new Select(listElement);
				String optionSelectedFromList=selectElement.getFirstSelectedOption().getText();
				System.out.println(optionSelectedFromList);

				selectElement.selectByVisibleText("Dr.");  // this uses the text of the option to be chosen
				optionSelectedFromList=selectElement.getFirstSelectedOption().getText();
				System.out.println(optionSelectedFromList);

				selectElement.selectByIndex(2);// this uses the position of the option to be chosen
				optionSelectedFromList=selectElement.getFirstSelectedOption().getText();
				System.out.println(optionSelectedFromList);

				selectElement.selectByValue("Frau");// this uses the value attribute of the option to be chosen
				optionSelectedFromList=selectElement.getFirstSelectedOption().getText();
				System.out.println(optionSelectedFromList);

				int selectedItemCOunt = selectElement.getAllSelectedOptions().size();
				System.out.println(selectedItemCOunt);

				
				int itemCount = selectElement.getOptions().size();
				System.out.println(itemCount);

				
			}

	

		
	}


