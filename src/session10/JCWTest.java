package session10;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class JCWTest {
	
	WebDriver driver; 
	String appURL="http://www.jcwhitney.com/";
	String yearOfMake ="2016";
	String make ="Ford";
	String model ="Mustang";
	String subModel ="GT";
	
	String xp_Year ="//select[@id='ymm_year']";
	String xp_Make ="//select[@id='ymm_make']";
	String xp_Model ="//select[@id='ymm_model']";
	String xp_Submodel ="//select[@id='ymm_submodel']";
	String xp_Search  = "//input[@id='ymm_submit']";
	String xp_headerText ="//h1";
	
	@Before
	
	public void setup() throws Exception {
		
		//System.setProperty("webdriver.chrome.driver" ,"./drivers/chromedriver");
		driver = new FirefoxDriver(); 
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.get(appURL);
	}
	
	
	
	@Test
	public void test() {
	
		WebElement yearElement = driver.findElement(By.xpath(xp_Year));
		Select yearList = new Select(yearElement);
		yearList.selectByVisibleText(yearOfMake);
		WebElement makeElement = driver.findElement(By.xpath(xp_Make));
		Select makeList = new Select(makeElement);	
		makeList.selectByVisibleText(make);
		WebElement modelElement = driver.findElement(By.xpath(xp_Model));
		Select modelList = new Select(modelElement);
		modelList.selectByVisibleText(model);

		WebElement submodelElement = driver.findElement(By.xpath(xp_Submodel));
		Select submodelList = new Select(submodelElement);
		
		submodelList.selectByVisibleText(subModel);
		
		driver.findElement(By.xpath(xp_Search)).click();
		
		String headerText =  driver.findElement(By.xpath(xp_headerText)).getText();
		String expectedHeaderText = yearOfMake + " "+ make+ " "+ model + " "+ subModel + " Parts & Accessories";
		//assertEquals("2015 Ford Mustang V6 Parts & Accessories", headerText);
		assertEquals(expectedHeaderText, headerText);
		
		
		

	}

	@After
	public void tearDown() throws Exception {
		driver.close(); // closes the current window in focus
		driver.quit();  // kills the current session running, meaning closes all windows related to the current session
	}

}



