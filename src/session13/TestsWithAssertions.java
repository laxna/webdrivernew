package session13;

	  
	  import org.testng.Assert;
	
	  import org.testng.annotations.Test;
	  public class TestsWithAssertions{
	    @Test
	    public void verifyStringsAreEqual() {
	  	  System.out.println("Test one begins...");
	  	  Assert.assertEquals("bharath", "bharath");
	  	  System.out.println("Test one ends...");

	    }
	    
	    @Test
	    public void verifyNumbersAreEqual() {
	  	  System.out.println("Test two begins...");
	  	  Assert.assertEquals(10, 10);

	  	  System.out.println("Test two ends...");
         System.out.println("1 2 3..");
	  	  
	    }
	    
	    
	    @Test
	    public void verifyBooleanValuesAreEqual() {
	  	  System.out.println("Test three begins...");
	  	  Assert.assertEquals(true, false);
	  	  System.out.println("Test three ends...");

	    }
	  

  }

