package session2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class HandlingElement {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.google.com/");
		
		String PageTitle = driver.getTitle();
		System.out.println(PageTitle);
		
		String PageSource = driver.getPageSource();
		
		
		String UrL = driver.getCurrentUrl();
		System.out.println(UrL);
		
		String WindowHandle = driver.getWindowHandle();
		System.out.println(WindowHandle);
		
		driver.close();
		
		
		
	
	}

}
