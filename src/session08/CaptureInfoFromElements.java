	package session08;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CaptureInfoFromElements {

	
	
	static String xpath_usernametextbox  = "//input[@name='username']";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		

		
			System.setProperty("webdriver.chrome.driver" ,"/Users/lakshanamishra/Documents/workspace/webdriver/lib/chromedriver");
			WebDriver driver = new ChromeDriver(); 
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

			driver.get("http://encreo.com/crm");
			
			driver.findElement(By.xpath(xpath_usernametextbox)).sendKeys("random username");
			
			String valueEntered= driver.findElement(By.xpath(xpath_usernametextbox)).getAttribute("value");
			System.out.println("Value entered in Username textbox ::"+ valueEntered);
			
			WebElement usernameTextbox = driver.findElement(By.xpath(xpath_usernametextbox));
			usernameTextbox.sendKeys("bharath");
			
			valueEntered = usernameTextbox.getAttribute("value");
			System.out.println("Value entered in Username textbox 2nd time ::"+ valueEntered);
			
			usernameTextbox.clear();
			valueEntered = usernameTextbox.getAttribute("value");

			System.out.println("Value entered in Username textbox after clearing ::"+ valueEntered);
			

	}

}
