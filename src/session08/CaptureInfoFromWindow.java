package session08;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CaptureInfoFromWindow {

	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		
		System.setProperty("webdriver.chrome.driver" ,"/Users/lakshanamishra/Documents/workspace/webdriver/lib/chromedriver");
		
		//WebDriver driver = new ChromeDriver(); 
		
		//driver = new FirefoxDriver();
		
		FirefoxDriver driver = new FirefoxDriver();
		
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

		driver.get("http://encreo.com/crm");
		String vtigerCRMURL = driver.getCurrentUrl();
		String vtigerLoginPageTitle = driver.getTitle();
		
		System.out.println("vtiger Login Page Title ::: "+ vtigerLoginPageTitle);
		System.out.println("vtiger Login Page URL ::: "+ vtigerCRMURL);

		driver.get("http://encreo.com");
		
		String encreoWebsiteHomePageTitle = driver.getTitle();
		String encreoURL = driver.getCurrentUrl();
		System.out.println("Encreo Website HOme Page Title ::: "+ encreoWebsiteHomePageTitle);
		System.out.println("Encreo Website HOme Page URL ::: "+ encreoURL);

		
		driver.get("http://linkedin.com");
		String linkedinURL = driver.getCurrentUrl();
		String linkedinWebsiteHomePageTitle = driver.getTitle();
		System.out.println("LinkedIn Website HOme Page Title ::: "+ linkedinWebsiteHomePageTitle);
		System.out.println("LinkedIn Website HOme Page URL ::: "+ linkedinURL);




	}

}
