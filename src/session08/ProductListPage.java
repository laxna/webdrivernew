package session08;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ProductListPage {
	
	private static String xpath_usernametextbox = "//input[@name='username']";
	private static String xpath_passwordtextbox = "//input[@name='password']";
	private static String xpath_signinButton = "//button[text()='Sign in']";
	private static String xpath_Products = "//strong[text() ='Products'][2]";

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver" ,"/Users/lakshanamishra/Documents/workspace/webdriver/lib/chromedriver");
		WebDriver driver = new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

		driver.get("http://encreo.com/crm");
		System.out.println("Login page title ::" + driver.getTitle());
		String loginPageTitle = driver.getTitle();
		
		WebElement usernameTextbox = driver.findElement(By.xpath(xpath_usernametextbox));
		usernameTextbox.clear();
		usernameTextbox.sendKeys("crmuser");
		
		
		WebElement passwordTextbox = driver.findElement(By.xpath(xpath_passwordtextbox));
		passwordTextbox.clear();
		passwordTextbox.sendKeys("crmuser");
		
		WebElement signinButton = driver.findElement(By.xpath(xpath_signinButton));
		signinButton.click();
		
		System.out.println("Home page title ::" + driver.getTitle());

		String actualHomePageTitle = driver.getTitle();
		String expectedHomePageTitle = "Home";
		
		if(actualHomePageTitle.equals(expectedHomePageTitle)){
			System.out.println("Login is successful");
		}else{
			System.err.println("Login is not successful.");
			System.out.println("Expected Page title ::"+ expectedHomePageTitle );
			System.out.println("Actual Page title ::" + actualHomePageTitle);
			
			WebElement Products = driver.findElement(By.xpath(xpath_Products));
		     Products.click();
			
			//driver.findElement(By.xpath("//strong[text() ='Products'][1]]")).click();
		
			//System.out.println("Product page displayed::");
			
			//driver.findElement(By.xpath("(//strong[text()='Products'])[2]")).click();
			
			System.out.println("Product page title ::"+driver.getTitle());
			
			String actualPageTitle = driver.getTitle();
			
			String expectedPageTitle = driver.getTitle();
			
			if(actualPageTitle.equals(expectedPageTitle)){ 
				System.out.println("Products"); 
				}else{ 
				System.err.println("link not found."); 
				}
				System.out.println("Expected Page title ::"+ expectedPageTitle ); 
				System.out.println("Actual Page title ::" + actualPageTitle); 
			
		}


	}

}
