package session08;

public class ComparingStrings {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		String expectedValue = "bharath";
		String actualValue = "bharath";
		
		// case sensitive matching
		if(actualValue.equals(expectedValue)){
			System.out.println("Expected and Actual values are matching");
		}else{
			System.err.println("Expected and Actual values are NOT matching");
			System.out.println("Expected value ::"+ expectedValue );
			System.out.println("Actual   value ::" + actualValue);
		}
		
		expectedValue = "bharath";
		actualValue = "BHARATH";
		
		// case insensitive matching
		if(actualValue.equalsIgnoreCase(expectedValue)){
			System.out.println("Expected and Actual values are matching ignoring case");
		}else{
			System.err.println("Expected and Actual values are NOT matching ignoring case");
			System.out.println("Expected value ::"+ expectedValue );
			System.out.println("Actual   value ::" + actualValue);
		}
		
		expectedValue = "bharath";
		actualValue = "bharath chetty";
		// partial matching
		if(actualValue.contains(expectedValue)){
			System.out.println("Actual value contains expected value");
		}else{
			System.err.println("Actual value doesn't expected value");
			System.out.println("Expected value ::"+ expectedValue );
			System.out.println("Actual   value ::" + actualValue);
		}

	}





	}


