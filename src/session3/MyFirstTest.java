package session3;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MyFirstTest {
	// this is the class

	public static void main(String[] args) {

		
		// this is main method
	
	
    /*FirefoxDriver firefox= new FirefoxDriver();
     firefox.get("http://linkedin.com");
     //firefox.findElement(By.name("q")).sendKeys("firefox");
     
     firefox.findElement(By.name("firstName")).sendKeys("Charlie");
 	firefox.findElement(By.name("lastName")).sendKeys("Chaplin");
 	firefox.findElement(By.name("firstName")).clear();
 	firefox.findElement(By.name("lastName")).clear();
 	firefox.findElement(By.id("first-name")).sendKeys("Junior 001");
 	firefox.findElement(By.id("last-name")).sendKeys("Junior 002");*/
 	
		//System.setProperty("webdriver.chrome.driver","/Users/lakshanamishra/Documents/workspace/webdriver/lib/chromedriver");
		//WebDriver driver = new ChromeDriver();
		
		WebDriver driver = new FirefoxDriver();
		
		/*googlechrome.get("http://linkedin.com");
		
	    googlechrome.findElement(By.className("className"));
		googlechrome.findElement(By.cssSelector("selector"));
		googlechrome.findElement(By.id("id"));
		googlechrome.findElement(By.linkText("linkText"));
		googlechrome.findElement(By.name("name"));
		googlechrome.findElement(By.partialLinkText(" partial linkText"));
		googlechrome.findElement(By.tagName("tagname"));
		googlechrome.findElement(By.xpath("xpathExpression"));*/
		
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		//maximize the window
		driver.manage().window().maximize();
		
		// go to vtiger crm login page
		driver.get("http://encreo.com/crm");
		
		//enter user name in username textbox
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("crmuser");
		
		// enter password in password textbox
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("crmuser");

		// click on signin button
		driver.findElement(By.xpath("//button[text()='Sign in']")).click();
		
		//click on Leads menu link
		driver.findElement(By.xpath("(//strong[text()='Leads'])[1]")).click();
		//  alternatively, try this also  :   //strong[text()='Leads']
		
		// click on Add Lead button
		driver.findElement(By.xpath("//strong[text()='Add Lead']")).click();
		
		//enter lead first name in lead first name textbox
		driver.findElement(By.xpath("//input[@name='firstname']")).sendKeys("Sanjay");
		// alternatively, you can try : //input[@id='Leads_editView_fieldName_firstname']

		
		// enter  lead last name in lead lastname textbox
		driver.findElement(By.xpath("//input[@id='Leads_editView_fieldName_lastname']")).sendKeys("Sharma");
		// alternatively, you can try : //input[@name='lastname']
		
		// click on save button
		driver.findElement(By.xpath("//strong[text()='Save']")).click();
		
		//fetch the full name text from the header element
		
		String fullName=driver.findElement(By.xpath("//h4")).getText();
		
		// use getText on an element, to fetch the text from the element. 
		// stored into a variable , fullName as above. text is of String data type, hence the variable type is String
		
		// print full name
		System.out.println("Full name ::"+ fullName);
		
		//click on User Name menu element
		driver.findElement(By.xpath("//strong[text()='Demo']")).click();
		
		// click signout link
		driver.findElement(By.xpath("//a[text()='Sign Out']")).click();
		

		// close the window
		driver.close();
		
		//stop driver
		driver.quit(); 
		


 	
		
		
		
	}
	
	
}